// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAAuYfLfQ5J8Va-mHaTUphG4ElfeagoceM",
  authDomain: "curd-react-be23a.firebaseapp.com",
  projectId: "curd-react-be23a",
  storageBucket: "curd-react-be23a.appspot.com",
  messagingSenderId: "944171985956",
  appId: "1:944171985956:web:d6b229a620ea3904567cb1",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);

export default db;
