import React, { useState, useEffect } from "react";
import { getDoc, doc } from "firebase/firestore";
import db from "../firebase/firebaseConfig";
export const LinkForm = (props) => {
  const initialStateValues = {
    url: "",
    name: "",
    description: "",
  };

  const [values, setValues] = useState(initialStateValues);

  const handleInputChanges = (e) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const getLinkById = async (id) => {
    const docs = await getDoc(doc(db, "links", id));
    setValues({...docs.data()})
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.addOrEditLink(values);
    setValues({ ...initialStateValues });
  };

  useEffect(() => {
    if (props.currentId === "") {
      setValues({ ...initialStateValues });
    } else {
      getLinkById(props.currentId);
    }
  }, [props.currentId]);

  return (
    <form className="card card-body" onSubmit={handleSubmit}>
      <div className="form-group input-group">
        <div className="input-group-text bg-light">
          <i className="material-icons">insert_link</i>
        </div>
        <input
          type="text"
          className="form-control"
          placeholder="https://someurl.xyz"
          name="url"
          onChange={handleInputChanges}
          value={values.url}
        />
      </div>
      <div className="form-group input-group">
        <div className="input-group-text bg-light">
          <i className="material-icons">create</i>
        </div>
        <input
          type="text"
          name="name"
          placeholder="Website Name"
          className="form-control"
          onChange={handleInputChanges}
          value={values.name}
        />
      </div>
      <div className="form-group">
        <textarea
          rows="3"
          className="form-control"
          placeholder="Write a Description"
          name="description"
          onChange={handleInputChanges}
          value={values.description}
        ></textarea>
      </div>
      <button className="btn btn-primary btn-block">{props.currentId === "" ? "Save": "Update"}</button>
    </form>
  );
};

export default LinkForm;
